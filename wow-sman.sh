## add packages for install
sudo apt install ansible pip libffi-dev curl git build-essential cmake libuv1-dev libssl-dev libhwloc-dev uuid-dev -y

## grab ansible playbook
git clone https://git.wownero.com/qvqc/ansible-remote-node
cd ansible-remote-node


## add Alliance Network Nodes here
cat <<'EOT' >> roles/wownerod/templates/wownerod.conf.j2

add-peer=v2admi6gbeprxnk6i2oscizhgy4v5ixu6iezkhj5udiwbfjjs2w7dnid.onion:34566
add-peer=iy6ry6uudpzvbd72zsipepukp6nsazjdu72n52vg3isfnxqn342flzad.onion:34566
add-peer=7ftpbpp6rbgqi5kjmhyin46essnh3eqb3m3rhfi7r2fr33iwkeuer3yd.onion:34566
add-peer=j7rf2jcccizcp47y5moehguyuqdpg4lusk642sw4nayuruitqaqbc7ad.onion:34566
add-peer=aje53o5z5twne5q2ljw44zkahhsuhjtwaxuburxddbf7n4pfsj4rj6qd.onion:34566
add-peer=nepc4lxndsooj2akn7ofrj3ooqc25242obchcag6tw3f2mxrms2uuvyd.onion:34566
add-peer=666l2ajxqjgj5lskvbokvworjysgvqag4oitokjuy7wz6juisul4jqad.onion:34566
add-peer=ty7ppqozzodz75audgvkprekiiqsovbyrkfdjwadrkbe3etyzloatxad.onion:34566
add-peer=ewynwpnprbgqllv2syn3drjdrqkw7ehoeg73znelm6mevvmpddexsoqd.onion:34566
add-peer=zao3w6isidntdbnyee5ufs7fyzmv7wzchpw32i3uo5eldjwmo4bxg2qd.onion:34566
add-peer=mqkiqwmhhpqtzlrf26stv7jvtaudbyzkbg3lttkmvvvauzgtxm62tgyd.onion:34566
add-peer=vq5bn3dhzorkpc6g6xi4vruylpdvfw6nrdf7ylyxudunsa4rzm4hduid.onion:34566
add-peer=oscqc6nia6ledut7ufpffej2kkjeuoonibwxudajg3su2w7p32xa.b32.i2p:34565
add-peer=72tbpgeczdtx2q2enbyaqcot7mghbnjenjkmdpyylrssqehr746a.b32.i2p:34565
add-peer=lrq65qrhpbt5voom2ncvowpes6kvobodkldhpuwhxlsrpugmgmlq.b32.i2p:34565
add-peer=rkel2qy7xv3cc5bnxfrzwgh3jvd4woagd4vlhr3qsdxy6cfkimnq.b32.i2p:34565

EOT

## setup pip dep
sudo pip install -r requirements.txt

## wipe and set inventory to localhost
> inventory.ini
echo "[all]" >> inventory.ini
echo "localhost ansible_connection=local" >> inventory.ini

## run playbook
ansible-playbook -i inventory.ini site.yaml -b

## getting local node info
i2pP2P=$( curl http://127.0.0.1:7070/?page=i2p_tunnels 2>&1 | grep -Eo "[a-zA-Z0-9./?=_%:-]*" | grep "34565" )
i2pRPC=$( curl http://127.0.0.1:7070/?page=i2p_tunnels 2>&1 | grep -Eo "[a-zA-Z0-9./?=_%:-]*" | grep "34568" )
torRPCP2P=$( sudo cat /var/lib/tor/wownero/hostname )

## add xmrig
cd ~
git clone https://github.com/xmrig/xmrig.git
mkdir xmrig/build
cd xmrig/build
cmake ..
make -j$(nproc)
sudo cp -a xmrig /usr/local/bin/xmrig

## add xmrig-proxy
cd ~
git clone https://github.com/xmrig/xmrig-proxy.git
mkdir xmrig-proxy/build
cd xmrig-proxy/build
cmake ..
make -j$(nproc)
sudo cp -a xmrig-proxy /usr/local/bin/xmrig-proxy

## display node information
> /tmp/WelcomeAlliance
cat <<EOT >> /tmp/WelcomeAlliance
Welcome to the Wownero Solo Miners Alliance Network

ADD RPC URLs TO
https://forum.wownero.com/t/solo-miners-alliance-network/746
AND
https://MONERO.FAIL/?crypto=wownero

YOUR I2P P2P ADDRESS
$i2pP2P
YOUR I2P RPC ADDRESS
$i2pRPC
YOUR TOR P2P/RPC ADDRESS ( :34566 / :34568 )
$torRPCP2P
EOT

cat /tmp/WelcomeAlliance
